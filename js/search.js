$(document).ready(function() {
    $('.submit1').click(function() {
        $(function() {

            $.getJSON('http://127.0.0.1:5500/data/goods.json', function(data) {
                let key = $('.search1').val();
                let numberOfFound = 0;
                if (key === "") {
                    alert('Задайте параметр поиска');
                    return false;
                }


                for (let i = 0; i < data.goods.length; i++) {
                    if (data.goods[i].name.toLowerCase().includes(String(key).toLowerCase())) {
                        numberOfFound++;
                        $('.found_goods').append('<br>' + '<div class="products" id="' + data.goods[i].id + '">' +
                            '<div class="img"><img src="' + data.goods[i].img + '"></img><p font-size="24">' + data.goods[i].name + '<br>' +
                            '</p></div><div class="text_product">$ ' +
                            data.goods[i].price + '</div><div><button class="button" id="' + data.goods[i].id +
                            '">В корзину</button></div></div>');
                    }
                }
                if (numberOfFound === 0) {
                    alert('Ничего не найдено');
                }
            });
        });
        $('.found_goods').empty();
        return false;
    });
});